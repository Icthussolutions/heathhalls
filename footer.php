<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

$container = get_theme_mod('understrap_container_type');
?>

<div class="divider-btm"></div>
<div class="wrapper footer-main" id="wrapper-footer">

    <div class="<?php echo esc_attr($container); ?>">

        <?php get_template_part('sidebar-templates/sidebar', 'footerfull'); ?>

        <div class="row">

            <div class="col-md-12">

                <footer class="site-footer" id="colophon">

                    <div class="site-info">
                        <span class="copy">
                            <?php _e('&copy; Copyright ', 'heathhalls');
                            echo date("Y");
                            _e(' Heath Citizens Association', 'heathhalls'); ?>
                        </span>
                        <span class="sep">|</span>
                        <span class="copy">
                            <?php _e('Registered Charity No: 524134 (Registered In England And Wales).', 'heathhalls'); ?>
                        </span>
                        <span class="copy">
                            <?php understrap_site_info(); ?>
                        </span>

                    </div><!-- .site-info -->

                </footer><!-- #colophon -->

            </div>
            <!--col end -->

        </div><!-- row end -->

    </div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</htm l>