<?php
/**
 * Template Name: Home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header('home');
$container = get_theme_mod('understrap_container_type');
?>

<?php if (have_rows('hero_section')) :

    while (have_rows('hero_section')) : the_row();

        // vars
        $logo = get_sub_field('logo');

        ?>
        <div class="hero" style='background-image: url("<?php the_sub_field('hero_background_image'); ?>")'>
            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
            <div class="<?php echo esc_attr($container); ?>">
                <h1><?php the_sub_field('hero_title'); ?></h1>
                <?php the_sub_field('hero_welcome_text'); ?>
                <a class="btn btn-lg" href="<?php the_sub_field('hero_button_link'); ?>"><?php the_sub_field('hero_button_text'); ?></a>
            </div>
        </div>

    <?php endwhile; ?>

<?php endif; ?>

<div class="wrapper" id="full-width-page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content">

        <?php if (have_rows('welcome_section')) :

            while (have_rows('welcome_section')) : the_row();

                ?>

                <div class="row">
                    <main class="site-main" id="main" role="main">

                        <div class="col-md-8 offset-md-2 welcome">
                            <h2><?php the_sub_field('welcome_title'); ?></h2>
                            <?php the_sub_field('welcome_text'); ?>
                        </div>

                    </main>


                </div>


            <?php endwhile; ?>

        <?php endif; ?>


        <?php if (have_rows('events_calendar')) :

            while (have_rows('events_calendar')) : the_row();

                ?>
                <section class="events">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2><?php the_sub_field('events_title'); ?></h2>
                        </div>
                    </div>

                    <?php the_sub_field('events_shortcode'); ?>

                    <div class="row">
                        <div class="col-md-4 offset-md-2 text-center">
                            <a class="btn btn-lg btn-secondary" href="<?php the_sub_field('front_hall_link'); ?>"><?php the_sub_field('front_hall_link_text'); ?></a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a class="btn btn-lg btn-secondary" href="<?php the_sub_field('rear_hall_link'); ?>"><?php the_sub_field('rear_hall_link_text'); ?></a>
                        </div>
                    </div>
                </section>





            <?php endwhile; ?>

        <?php endif; ?>



    </div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer();
?>