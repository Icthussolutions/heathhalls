<?php
/**
 * Template Name: Full Width
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod('understrap_container_type');
?>

<div class="wrapper" id="full-width-page-wrapper">

  <div class="<?php echo esc_attr($container); ?>" id="content">

    <section>
      <main class="site-main" id="main" role="main">
        <div class="row">
          <div class="col-md-8">

            <?php
            if (have_posts()) :
              while (have_posts()) : the_post(); ?>

                <?php get_template_part('loop-templates/content', 'page'); ?>

              <?php

            endwhile;
          endif;
          ?>

          </div>

          <?php get_template_part('sidebar-templates/sidebar', 'right'); ?>

        </div>
      </main>
    </section>


  </div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer();
?>