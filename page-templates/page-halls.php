<?php
/**
 * Template Name: Halls Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod('understrap_container_type');
?>

<div class="wrapper" id="full-width-page-wrapper">

  <div class="<?php echo esc_attr($container); ?>" id="content">

    <div class="row details">


      <div class="col-md-10">
        <?php
        $images = get_field('hall_gallery');
        if ($images) : ?>
          <div class="gallery">
            <?php foreach ($images as $image) : ?>
              <a href="<?php echo $image['url']; ?>" target="_blank" class="thumbnail">
                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php the_title(); ?>" />
              </a>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
      <div class="col-md-2">
        <?php the_field('hall_facilities'); ?>
      </div>


    </div>

    <section class="events">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-center"><?php the_field('hall_events_title'); ?></h2>


          <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('loop-templates/content', 'page'); ?>

          <?php endwhile; ?>

        </div>
      </div>

    </section>


  </div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php get_footer();
?>